// In App.js in a new project

import React from 'react';
import { View, Text } from 'react-native';
import { StackNavigator } from 'react-navigation';

//Import Screens
import StartScreen from './src/scenes/start';
import FilterPlacesScreen from './src/scenes/filterPlaces';
import PlaceScreen from './src/scenes/place';
import MapScreen from './src/scenes/map';
import ReadQRScreen from './src/scenes/readQR';
import GeolocationScreen from './src/scenes/geolocation';
import OwnerRegisterScreen from './src/scenes/ownerRegister';
import OwnerLoginScreen from './src/scenes/ownerLogin';
import OwnerPlatformScreen from './src/scenes/ownerPlatform';
import OwnerChangePassScreen from './src/scenes/ownerChangePassword';
import CategoriesScreen from './src/scenes/categories';

export default StackNavigator({
  OwnerLogin: {
    screen: OwnerLoginScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
  Start: {
    screen: StartScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
  Geolocation: {
    screen: GeolocationScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
  Categories: {
    screen: CategoriesScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
  FilterPlaces: {
    screen: FilterPlacesScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
  Place: {
    screen: PlaceScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
  Map: {
    screen: MapScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
  OwnerRegister: {
    screen: OwnerRegisterScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
  OwnerPlatform: {
    screen: OwnerPlatformScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
  OwnerChangePass: {
    screen: OwnerChangePassScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
  ReadQRS: {
    screen: ReadQRScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null,
      gesturesEnabled: false
    }
  },
});
