import React from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';

var {height, width} = Dimensions.get('window');

function Filter(props) {
  return (
    
        <View style={{marginHorizontal:3, marginVertical:5, justifyContent:'center', alignItems:'center', 
                        borderRadius:8, height:50, backgroundColor:props.selected?'#1b8c71':'white', borderColor:'#E8EDF2', borderWidth:1}}>
            <Text style={{fontSize:18, marginVertical:10, marginHorizontal:10, color:props.selected?'white':'grey'}}>{props.title}</Text>
        </View>
    
  )
}

const styles = StyleSheet.create({
  
})

export default Filter
