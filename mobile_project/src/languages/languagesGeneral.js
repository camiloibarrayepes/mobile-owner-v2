const languagesGeneral = {
    //OWNER
    ownerLogin: "Owner Login",
    pleaseEnterYourAcc: "Por favor ingrese sus datos de ingreso",
    register: "Registrar",
    signIn: "Ingresar",
    ownerRegister: "Registro para Owner",
    pleaseFillForm: "Por favor complete los siguientes datos",
    storeName: "Nombre establecimiento",
    storeAddress: "Dirección",
    setLocation: "Definir ubicación",
    hasBeenRegistered: "Ha sido registrado",
    success: "Éxito",
    automaticLogin: "Login automático",
    manualLogin: "Login manual",
    changePass: "Cambiar contraseña",
    actualPass: "Contraseña actual",
    newPass: "Nueva Contraseña",
    repeatNewPass: "Repetir nueva contraseña",
    //
    congestionInfo: "Tiempo por congestión",
    detallesPlace: "Detalles de la ubicación",
    viewLocation: "Ver ubicación"
    
};


export default languagesGeneral;
