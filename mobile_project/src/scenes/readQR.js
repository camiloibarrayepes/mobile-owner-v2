import React, { Component, Fragment } from 'react';
import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from './scanStyle'
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions,
    StatusBar,
    Picker,
    ActivityIndicator,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    Input,
    Alert,
    Button,
    SafeAreaView,
    TextInput
} from 'react-native';

import images from '@assets/imageGeneral'
import RNPickerSelect from 'react-native-picker-select';
import apiRoute from '@routes/apiRoute';
import AsyncStorage from '@react-native-community/async-storage';

var LAT_USER = 0
var LONG_USER = 0
var DATE = 0
var ID_PERSON = ""


class Scan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scan: false,
            ScanResult: false,
            result: null,
            scan: true,
            id_person: ""
        };
    }

    goBack() {
        const { navigation } = this.props;
        navigation.goBack();
    }

    async componentWillMount() {
        const id_person = await AsyncStorage.getItem('id_person');
        console.warn("id_person", id_person)
        this.setState({id_person: id_person})
        ID_PERSON = id_person

        const lat_user = await AsyncStorage.getItem('lat_user');
        const long_user = await AsyncStorage.getItem('long_user');

        LAT_USER = parseFloat(lat_user)
        LONG_USER = parseFloat(long_user)

        console.warn("DATE", Date.now())
        DATE = Date.now()
        
    }

    onSuccess = (e) => {
        //const check = e.data.substring(0, 4);
        //console.log("check", check)
        //console.log('scanned data' + check);
        this.setState({
            result: e,
            scan: false,
            ScanResult: true
        })
        console.warn("e.data", e.data)
        console.warn("e", e)
        this.callAPI(e.data)
        // if (check === 'http') {
        //     Linking
        //         .openURL(e.data)
        //         .catch(err => console.error('An error occured', err));


        // } else {
        //     this.setState({
        //         result: e,
        //         scan: false,
        //         ScanResult: true
        //     })
        // }

    }

    callAPI(data){
        console.warn("ID_PERSON", ID_PERSON, "LAT_USER", LAT_USER, "LONG_USER", LONG_USER, "DATE", DATE, "token", data)
        console.log("ID_PERSON", ID_PERSON, "LAT_USER", LAT_USER, "LONG_USER", LONG_USER, "DATE", DATE, "token", data)
        var finalUrl = apiRoute.api + "users/register-visit"
        try{
            fetch(finalUrl, {
                    method: 'POST',
                    headers:{
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        id_person: ID_PERSON,
                        location: {
                            longitude: LAT_USER,
                            latitude: LONG_USER,
                        },
                        date: DATE,
                        token: data
                    }),
                    }).then((response) => {
                    console.log("responseQR", response)
                    console.warn("responseQR", response)
                    if(response.status == 200 || response.status == 201){
                        response.json().then((json) => {
                            console.log("jsonQR", json)
                            console.warn("jsonQR", json)
                            Alert.alert(
                                language.success,
                                json.message,
                                [
                                  {
                                    text: "OK",
                                    onPress: () => this.props.navigation.navigate('Categories')
                                  }
                                ],
                                { cancelable: false }
                              );
                        })
                    }
                })
            }catch(error){
                console.warn(error)
            }
        } 

    activeQR = () => {
        this.setState({
            scan: true
        })
    }
    scanAgain = () => {
        this.setState({
            scan: true,
            ScanResult: false
        })
    }
    render() {
        const { scan, ScanResult, result } = this.state
        const { state, goBack } = this.props.navigation;
        return (
          <View style={{backgroundColor:'white', flex:1}}>
            <SafeAreaView>
                <TouchableOpacity 
                    onPress={() => this.goBack()}
                    style={{marginLeft:20, marginTop:20}}>
                    <Image style={{ width:25, height:25}} source={images.exitIcon} />      
                </TouchableOpacity>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                    <View>
                        <View style={{flexDirection:'column', alignItems:'center', marginHorizontal:20}}>
                            <Text style={{fontSize:22}}>QR Read</Text>
                            <Text style={{marginTop:10, marginBottom:20, textAlign:'center', color:'grey'}}>Please focus the QR Code</Text>
                            
                            {scan &&
                                <QRCodeScanner
                                    reactivate={true}
                                    showMarker={true}
                                    vibrate={Platform.OS == "android"}
                                    ref={(node) => { this.scanner = node }}
                                    onRead={this.onSuccess}
                                    /*topContent={
                                        <Text style={styles.centerText}>
                                            Go to <Text style={styles.textBold}>wikipedia.org/wiki/QR_code</Text> on your computer and scan the QR code to test.</Text>
                                    }
                                    bottomContent={
                                        <View>
                                            <TouchableOpacity style={styles.buttonTouchable} onPress={() => this.scanner.reactivate()}>
                                                <Text style={styles.buttonTextStyle}>OK. Got it!</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity style={styles.buttonTouchable} onPress={() => this.setState({ scan: false })}>
                                                <Text style={styles.buttonTextStyle}>Stop Scan</Text>
                                            </TouchableOpacity>
                                        </View>

                                    }*/
                                />
                            }
                            {ScanResult &&
                            <Text> Restul QR: {result.data}</Text>
                            }
                        </View>
                    </View>
                </TouchableWithoutFeedback>
              </SafeAreaView>
          </View>
        );
      }
}



export default Scan;