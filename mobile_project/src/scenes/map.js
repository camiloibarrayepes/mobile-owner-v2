
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image, 
  Alert,
  ActivityIndicator
} from 'react-native';

import images from '@assets/imageGeneral';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

const { width, height } = Dimensions.get('window');

var LAT_USER = 0
var LONG_USER = 0
const ASPECT_RATIO = width / height;
var LATITUDE = 0;
var LONGITUDE = 0;
var CATEGORY = "";
const LATITUDE_DELTA = 0.05;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;


class DefaultMarkers extends React.Component {

    goBack() {
        const { navigation } = this.props;
        navigation.goBack();
    }
    
    watchID: ?number = null;

    constructor(props) {
        super(props);
        /// DATA SENT
        //*
        //*
        /// USER LOCATION
        const lat_user =  this.props.navigation.getParam('lat_user', 'nothing sent')
        const long_user =  this.props.navigation.getParam('long_user', 'nothing sent')
        LAT_USER = lat_user
        LONG_USER = long_user
        console.warn("LAT_USER " + lat_user);
        console.warn("LONG_USER " + long_user);
        //*
        //*
        /// PLACE LOCATION
        const info =  this.props.navigation.getParam('info', 'nothing sent')
        console.warn("LONG " + info.location.longitude);
        console.warn("LAT " + info.location.latitude);
        //*
        //*
        /// SET DATA PLACE
        CATEGORY = info.category
        LATITUDE = info.location.latitude
        LONGITUDE = info.location.longitude
        
        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              },
            regionUser: {
                latitude: LAT_USER,
                longitude: LONG_USER,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
          markers: [],
        };
      }

      render() {
        return (
          <View style={styles.container}>   
            {LATITUDE == 0 ?  
            <ActivityIndicator style={{marginBottom:height/2-30}} size="large" color="grey" />
            :
            <MapView
              provider={this.props.provider}
              style={styles.map}
              initialRegion={this.state.region}
              onPress={e => console.log(e)}>
                <Marker
                    key="2"
                    coordinate={this.state.region}
                    pinColor= "red">
                    <Image
                        source={CATEGORY=="Pharmacie"?images.pharmacyIcon:images.shopIcon}
                        style={{height: 75, width: 75}} />
                </Marker>
                <Marker
                    key="2"
                    coordinate={this.state.regionUser}
                    pinColor= "red">
                    <Image source={images.houseIcon} style={{height: 75, width: 75}} />
                </Marker>
            </MapView>
            }
            <TouchableOpacity 
            onPress={() => this.goBack()}
            style={{alignItems:'center', justifyContent: 'center', 
                                    backgroundColor:'white', width:40, height:40, 
                                    borderRadius:50, position:'absolute', 
                                    left:20, top:50,shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 4,
                                    },
                                    shadowOpacity: 0.30,
                                    shadowRadius: 4.65,
                                    elevation: 8}}>
                <Image style={{marginLeft:-4, width:25, height:25}} source={images.backIcon} />
            </TouchableOpacity>
          </View>
        );
      }
    }


DefaultMarkers.propTypes = {
    provider: ProviderPropType,
  };
  
  const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
    bubble: {
      backgroundColor: 'rgba(255,255,255,0.7)',
      width: width*0.9,
      alignItems: 'center',
      justifyContent: 'center'
    },
    latlng: {
      width: 200,
      alignItems: 'stretch',
    },
    button: {
      width: 80,
      paddingHorizontal: 12,
      alignItems: 'center',
      marginHorizontal: 10,
    },
    buttonContainer: {
      flexDirection: 'row',
      backgroundColor: 'white',
      borderRadius: 20,
      height:110,
      width: width*0.9,
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom:10,
      borderBottomWidth: 0,
      shadowColor: 'gray',
      shadowOffset: { width: 0, height: 3 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 1,
    },
    buttonConfirm: {
      flexDirection: 'row',
      backgroundColor: '#F3C610',
      height:60,
      marginBottom: 80,
      width: width*0.9,
      borderRadius: 10,
      alignItems: 'center',
      justifyContent: 'center',
      shadowColor: 'gray',
      shadowOffset: { width: 0, height: 3 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 1
    }
  });
  
  export default DefaultMarkers;