import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ActivityIndicator,
  ScrollView,
  Modal,
  Button,
  SafeAreaView,
  TextInput,
} from 'react-native';
import SideMenu from 'react-native-side-menu';
import Menu from './menu';
import MenuDos from './menuDos';
import RNPickerSelect from 'react-native-picker-select';
import images from '@assets/imageGeneral';
import apiRoute from '@routes/apiRoute';
import Filter from '../components/Filter/filter'
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';

var HASNOTCH = false
var LAT_USER = 0
var LONG_USER = 0
var EXISTS_TOKEN = ""
const { width, height } = Dimensions.get('window');

const colors = {
  red:'#DF6E6E',
  green:'#b7eb8f',
  yellow:'#DF6E6E',
  grey:'#CACACA',
  blue: '#2291E8'
}

export default class Categories extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      selectedItem: 'About',
      pruebaItem: 'Pharmacie',
      placesVector: [],
      loading: true,
      radius: "500m",
      establishments: [],
      valueSearch: "",
      modalConfirData: false
    };
  }

componentDidMount() {
  this.getData()
  token =  this.props.navigation.getParam('token', 'nothing sent')
  console.log("token sent", token)
  this.getEstablishments(token)
}

getData = async () => {
  try {
    const tokenStorage = await AsyncStorage.getItem('token')
    console.warn("tokenStorage", tokenStorage)
  } catch(e) {
    //error
  }
}

getEstablishments(token){
  var finalUrl = apiRoute.api + "owners/"
  try{
      fetch(finalUrl,{
          method: 'GET',
          headers:{
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
          },
          }).then((response) => {
          if(response.status == 200 || response.status == 201){
              this.setState({loading: false})
              response.json().then((json) => {
              if(!json.success) {
                  alert(json.error)
              } else {
                  console.log(json.establishments)
                  console.log("xcv", json.establishments.length)
                  establishments = json.establishments
                  this.setState({establishments: establishments})
              }
              console.log("establishmentsx2", establishments)
              })
          } else {
            alert("Ha ocurrido un error")
            this.setState({loading: false})
          }
          })
      }catch(error){
          console.warn(error)
      }
  } 

storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    // saving error
    console.warn("cath error", e)
  }
}

  onSelect = data => {
    
    // if(data.selected) {
    //   this.setState({
    //     showFinalPlace: true
    //   })
    // }
    this.setState(data);

    this.getPlaces("Pharmacie", 48.8588377, 2.2770203, "500m")
  };

  toggle() {
      console.warn("1")
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, value)
    } catch (e) {
      // saving error
      console.warn("cath error", e)
    }
  }

  clearItem = async () => {
    this.storeData('token', "")
    this.props.navigation.navigate('OwnerLogin')
  };

  onMenuItemSelected = item => {
    console.warn("item", item)
    if(item=="DETECT_LOCATION") {
      this.props.navigation.navigate('Geolocation')
    } else if(item=="ESTABLISHMENTS") {
      
    } else  if(item=="SCAN_QR") {
      this.props.navigation.navigate('ReadQRS')
    } else if (item=="OWNER_LOGIN") {
      this.props.navigation.navigate('OwnerLogin')
    } else if (item=="LOG_OUT"){
      console.warn("LOG_OUT")
      this.clearItem()
    } else if (item=="OWNER_PASS"){
      this.props.navigation.navigate('OwnerChangePass')
    }
    this.setState({
      isOpen: false,
      selectedItem: item,
    });
  }

  searchFunction = value => {
    console.warn("value", value)
    this.setState({valueSearch: value})
  }

  async componentWillMount() {
  }
  
  render() {
  
    const menu = (EXISTS_TOKEN == "" || EXISTS_TOKEN == null) ? <Menu onItemSelected={this.onMenuItemSelected} /> : <MenuDos onItemSelected={this.onMenuItemSelected} />;
    //const menu = <MenuDos onItemSelected={this.onMenuItemSelected} />;

    const placeholder = {
        label: 'Select a sport..',
        value: null,
        color: '#9EA0A4',
    };
    const placeholderDistance = {
        label: 'Select a distance..',
        value: null,
        color: '#9EA0A4',
    };

    return (
      <SafeAreaView>
          <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
          <Text style={{marginTop:height}}></Text>
        <SideMenu
          menu={menu}
          isOpen={this.state.isOpen}
          onChange={isOpen => this.updateMenuState(isOpen)}
        >
          <View style={styles.container}>
                <View style={{marginTop: DeviceInfo.hasNotch() ? 30 : 10, width: width, height: 70, backgroundColor:'#FAFAFA', borderBottomWidth:0.25, borderBottomColor:'grey'}}>
                    <View>
                        <View style={{flexDirection:'row'}}>
                            <View style={{width:width*0.1,  marginTop:31}}>
                                <TouchableOpacity 
                                onPress={()=>{this.updateMenuState(!this.state.isOpen)}}
                                style={{marginLeft:10}}>
                                    <View style={{backgroundColor:'grey', height:2, width:23}} />
                                    <View style={{backgroundColor:'grey', marginTop:5, height:2, width:23}} />
                                    <View style={{backgroundColor:'grey', marginTop:5, height:2, width:23}} />
                                    <View style={{backgroundColor:'grey', marginTop:5, height:2, width:23}} />
                                </TouchableOpacity>
                            </View>
                            <View style={{alignItems:'center', borderRadius:5, justifyContent:'center', marginTop:35, width: width*0.8}}>
                              <Text style={{fontWeight:'bold', fontSize:16}}>Establecimientos</Text>
                            </View>
                            <View style={{width:width*0.1, marginTop:30}}>
                                <TouchableOpacity
                                onPress={()=>{this.props.navigation.navigate('ReadQRS')}}
                                >
                                    <Image style={{width:32, height:32}} source={images.qrIconB} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
                {/*========================== END TAB BAR ====================================*/}
                <View style={{flex: 1}}>
                  <View>
                      <View style={{flexDirection:'column', alignItems:'center', marginHorizontal:10, marginBottom: 80}}>
                          {
                          !this.state.loading ?
                          <ScrollView style={{marginBottom:50, marginTop: 20}}>
                            {/*====== ITEMS ======*/}
                            {
                            this.state.establishments.map((item) => {
                                  return (
                                      <TouchableOpacity
                                      onPress={() => {this.setState({
                                                        modalConfirData:true,
                                                        estSelectedName: item.name,
                                                        estSelectedID: item.id
                                                      })
                                                    }
                                                  }
                                      style={{borderColor:'#CACACA', borderWidth:0.5, width: width*0.92}}
                                      >
                                          <View style={{flex:1, paddingVertical:10, marginLeft:10}}>
                                              <Text>{item.name}</Text>
                                          </View>        
                                      </TouchableOpacity>
                                  );
                              })
                            }
                          </ScrollView>
                          :
                          <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                          } 
                      </View>
                  </View>
                </View>


              {/*============ MODAL CONFIRMACION DATOS PARA PAGO =============*/}

              <Modal 
                  visible={this.state.modalConfirData} 
                  animationType='fade' 
                  transparent={true}  >
                
                <View style={{flex: 1,alignItems: 'center', justifyContent:'center', flexDirection: 'column',  backgroundColor: 'rgba(0, 0, 0, 0.8)',}}>
                  
                  <View style={{flexDirection:'column'}}>
                    <View style={{alignItems:'center', width: 300, height: 330, backgroundColor:'white'}}>
                      <Image style={{marginTop:20, width:260, height:250}} source={images.pharmacie2} />
                      <Text style={{marginTop:10}}>
                        {this.state.estSelectedName}
                      </Text>
                    </View>
                    <View style={{justifyContent:'space-between', flexDirection:'row', marginTop:20}}>
                        <TouchableOpacity style={{justifyContent:'center', alignItems:'center', backgroundColor:colors.blue, borderRadius:50, width:60, height:60}}>
                          <Image style={{ width:30, height:30}} source={images.editIcon} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{justifyContent:'center', alignItems:'center', backgroundColor:colors.red, borderRadius:50, width:60, height:60}}>
                          <Image style={{ width:30, height:30}} source={images.deleteIcon} />
                        </TouchableOpacity>
                        <TouchableOpacity 
                        onPress={()=>{this.setState({modalConfirData:false})}}
                        style={{justifyContent:'center', alignItems:'center', backgroundColor:colors.grey, borderRadius:50, width:60, height:60}}>
                          <Image style={{ width:30, height:30}} source={images.closeIcon} />
                        </TouchableOpacity>
                    </View>
                  </View>
                </View>
                
              </Modal>

              
          </View>
        </SideMenu>
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });