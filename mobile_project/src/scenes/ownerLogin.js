import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ActivityIndicator,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  Input,
  Alert,
  Button,
  SafeAreaView,
  TextInput
} from 'react-native';

import images from '@assets/imageGeneral'
import RNPickerSelect from 'react-native-picker-select';
import apiRoute from '@routes/apiRoute';
import language from '@languages/languagesGeneral'
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('window');



const places = [
    {
      label: 'Pharmacie',
      value: 'Pharmacie',
    },
    {
      label: 'Hospital',
      value: 'Hospital',
    },
    {
      label: 'Grocery',
      value: 'Grocery',
    },
    {
      label: 'Bakery',
      value: 'Bakery',
    },
    {
      label: 'Supermarket',
      value: 'Supermarket',
    },
]

export default class OwnerLogin extends Component {

  constructor(props) {
    super(props);
    this.state = {
        email: "",
        storeName: "",
        address: "",
        password: "",
        hidePass: true,
        loading: false
    };
  }

componentDidMount() {
    
}

storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    // saving error
    console.warn("cath error", e)
  }
}

goBack() {
    const { navigation } = this.props;
    navigation.goBack();
}

async verifyData(){

    const {email,password} = this.state;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    const userEmail = email
    
    if(userEmail == undefined || userEmail == ""){
        alert("Email requerido")
    } else if(password == undefined || password == ""){
        alert("Password requerido")
    }  else if(userEmail != undefined && userEmail != "" && reg.test(userEmail) === false) {
        alert('Email syntax invalid, it must to be like this: myemail@email.com');
    } else{
        const emailLower = userEmail.toLowerCase();
        this.setState({email: email})
        this.loginUser(emailLower, password)
    }
}

    loginUser(email, password){
    this.setState({loading: true})
    var finalUrl = apiRoute.api + "owners/signin"
    try{
        fetch(finalUrl,{
            method: 'POST',
            headers:{
            Accept: 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
            email: email,
            password: password,
            }),
            }).then((response) => {
            if(response.status == 200 || response.status == 201){
                this.setState({loading: false})
                response.json().then((json) => {
                console.log("LOG", json)
                if(!json.success) {
                    alert(json.error)
                } else {
                    //alert("Success Login " + email)
                    console.log("json", json.token)
                    this.storeData('token', json.token)
                    this.storeData('userPass', password)
                    this.props.navigation.navigate('Categories', {token: json.token})
                }
                })
            } else {
              this.setState({loading: false})
              alert("Hubo un problema")
            }
            })
        }catch(error){
            console.warn(error)
        }
    } 

  render() {
    const { state, goBack } = this.props.navigation;
    const emailValue =  this.props.navigation.getParam('emailValue', '')
    const passwordValue =  this.props.navigation.getParam('passwordValue', '')
    
    return (
      <View style={{backgroundColor:'white', flex:1}}>
        <SafeAreaView>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View>
                    <View style={{flexDirection:'column', alignItems:'center', marginTop:40, marginHorizontal:20}}>
                        <Text style={{fontSize:22}}>{language.ownerLogin}</Text>
                        <Text style={{marginTop:10, textAlign:'center', color:'grey'}}>{language.pleaseEnterYourAcc}</Text>
                        {
                            /*
                            * INPUT DATA
                            * FIELDS
                            */
                        }
                        <TextInput 
                            autoCorrect={true}
                            onChangeText={email => this.setState({email})}
                            placeholder="Email"
                            style={{color:'grey',paddingHorizontal:15, marginTop:20, borderRadius:5, borderWidth:0.3, borderColor:'#CCC', height: 40, width: width*0.8, backgroundColor:'#FCFCFC'}}>
                        </TextInput>
                        <View style={{alignItems:'center', flexDirection:'row'}}>
                        <TextInput 
                            onChangeText={password => this.setState({password})}
                            placeholder="Password"
                            secureTextEntry={this.state.hidePass?true:false}
                            style={{color:'grey',paddingHorizontal:15, marginTop:20, borderRadius:5, borderWidth:0.3, borderColor:'#CCC', height: 40, width: width*0.8, backgroundColor:'#FCFCFC'}}>
                        </TextInput>
                        <TouchableOpacity style={{bottom:8, position:'absolute', right: 15}}
                            onPress={()=>{this.setState({hidePass: !this.state.hidePass})}}
                        >
                            {this.state.hidePass ? 
                            <Image style={{width:25, height:25}} source={images.eyeIcon} />
                            :
                            <Image style={{width:25, height:25}} source={images.eyeLock} />
                            }
                        </TouchableOpacity>
                        </View> 
                        <TouchableOpacity 
                        onPress={()=>{this.verifyData()}}
                        style={{marginTop:40, borderRadius:5, alignItems:'center', justifyContent:'center', height:40, width:width*0.8, backgroundColor:'#3796F0'}}>
                            {
                              this.state.loading ? 
                              <ActivityIndicator size="small" color="white" />
                              :
                              <Text style={{color:'white'}}>{language.signIn}</Text>
                            }
                        </TouchableOpacity>
                        <TouchableOpacity 
                          style={{marginTop:10}}
                          onPress={()=>{this.props.navigation.navigate('OwnerRegister')}}
                          >
                          <Text style={{color:'grey'}}>Crear una cuenta</Text>
                        </TouchableOpacity>
                    </View>
                    
                </View>
            </TouchableWithoutFeedback>
          </SafeAreaView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });