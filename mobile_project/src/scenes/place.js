import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ActivityIndicator,
  ScrollView,
  Button,
  SafeAreaView,
} from 'react-native';
import SideMenu from 'react-native-side-menu';
import Menu from './menu';
import RNPickerSelect from 'react-native-picker-select';
import images from '@assets/imageGeneral';
import apiRoute from '@routes/apiRoute';
import language from '@languages/languagesGeneral'

var LAT_USER = 0
var LONG_USER = 0

const { width, height } = Dimensions.get('window');

const colors = {
    red:'#DF6E6E',
    green:'#b7eb8f',
    yellow:'#DF6E6E',
    grey:'#DF6E6E'
}
export default class Place extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      selectedItem: 'About',
      pruebaItem: 'Pharmacie',
      placesVector: [],
      loading: true,
      radius: "500m"
    };
  }

componentDidMount() {
    this.shuffleArray()
    this.getPlaces("Pharmacie", 48.8588377, 2.2770203, this.state.radius)
    console.warn("this.state.placesVector", this.state.placesVector.length)
}

goBack() {
    const { navigation } = this.props;
    navigation.goBack();
    navigation.state.params.onSelect({ selected: true });
}

shuffleArray() {
    var ro = Math.floor(Math.random() * (3));
    this.setState({randomN:ro})
    console.warn("random", ro)
}

selectBK(ro){
    switch(ro){
      case 0:
        return images.pharmacie1
        break;
      case 1:
        return images.pharmacie2
        break;
      case 2:
        return images.pharmacie3
        break;
  }
}

getPlaces(category, lat, long, radius) {
    //GET request 
    /*
    Radius:
    -  500m = 500m
    -  1km = 1%20km
    -  2km = 2%20km
    */
    console.warn("ENTRA")
    this.setState({loading: true})
    var finalUrl = apiRoute.api + "establishment?lat="+lat+"&lng="+long+"&radius="+radius+"&category="+category+"&offset=0&limit=10"
    console.warn("finalUrl", finalUrl)

    fetch(finalUrl, {
        method: 'GET'
        //Request Type 
    })
    .then((response) => response.json())
    //If response is in json then in success
    .then((responseJson) => {
        //Success 
        //console.warn("responseJson", responseJson.values)
        responseJson.values.forEach(element => {
            //console.warn("element", element)
        });
        this.setState({placesVector: responseJson.values, loading: false})
        console.warn("placesVector", this.state.placesVector)
    })
    //If response is not in json then in error
    .catch((error) => {
        //Error 
        //alert(JSON.stringify(error));
        console.error(error);
    });
  }

  toggle() {
      console.warn("1")
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    console.warn("2")
    this.setState({ isOpen });
  }

  onMenuItemSelected = item =>
    this.setState({
      isOpen: false,
      selectedItem: item,
    });


  render() {

    const { state, goBack } = this.props.navigation;
    ///Data sent
    //*
    //*
    /// USER LOCATION
    //*
    const lat_user =  this.props.navigation.getParam('lat_user', 'nothing sent')
    const long_user =  this.props.navigation.getParam('long_user', 'nothing sent')
    console.warn("LAT_USER " + lat_user);
    console.warn("LONG_USER " + long_user);
    LAT_USER = lat_user
    LONG_USER = long_user
    //*
    /// INFO PLACE
    //*
    const info =  this.props.navigation.getParam('info', 'nothing sent')
    console.warn("PROPS " + info.name);

    const menu = <Menu onItemSelected={this.onMenuItemSelected} />;
    const placeholder = {
        label: 'Select a sport..',
        value: null,
        color: '#9EA0A4',
    };
    const placeholderDistance = {
        label: 'Select a distance..',
        value: null,
        color: '#9EA0A4',
    };

    return (
      <View>
          <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
          <View style={styles.container}>
                <View style={{width: width, marginTop:10, height: 70, backgroundColor:'#FAFAFA', borderBottomWidth:0.25, borderBottomColor:'grey'}}>
                    <View>
                        <View style={{flexDirection:'row'}}>
                            <View style={{width:width*0.1,  marginTop:40}}>
                                <TouchableOpacity 
                                onPress={() => this.goBack()}
                                style={{marginLeft:5, marginTop:-5}}>
                                    <Image style={{width:25, height:25}} source={images.backIcon} />
                                </TouchableOpacity>
                            </View>
                            <View style={{alignItems:'center', borderRadius:5, justifyContent:'center', marginTop:38, width: width*0.8}}>
                                <Text style={{fontWeight:'bold', fontSize:18}}>{info.name}</Text>
                            </View>
                            <View style={{width:width*0.1, marginTop:35}}>
                                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('ReadQRS')}}>
                                    <Image style={{width:30, height:30}} source={images.qrIconB} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
                <ScrollView scrollEnabled={height>600?false:true}>
                    <View style={{marginHorizontal:10, marginVertical:10}}>
                        <Image style={{width:width*0.95, height:250}} source={this.selectBK(this.state.randomN)} />
                        <TouchableOpacity
                        style={{borderRadius:20, marginBottom:10, flexDirection:'row', backgroundColor:'white', paddingVertical:10, paddingHorizontal:15, position:'absolute', bottom:0, right:5}}
                        onPress={()=>{console.warn("TOUCH"), this.props.navigation.navigate('Map', { info: info, lat_user: LAT_USER, long_user: LONG_USER})}}
                        >
                            <Text>{language.viewLocation}</Text>
                            <Image style={{left:5, width:15, height:15}} source={images.iconMap} />
                            
                        </TouchableOpacity>
                    </View>
                    <View  style={{borderWidth:0.5, borderColor:'grey', marginHorizontal:10, marginTop:10}}>
                    <View 
                    style={{position:'absolute', top:-7, left:15}}>
                        <View style={{backgroundColor:'white', paddingHorizontal:4}}>
                        <Text style={{color:'#1b8c71'}}>{language.detallesPlace}</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                        <View style={{flex:0.9, paddingHorizontal:10, paddingBottom:10, paddingTop:20, flexDirection: 'column'}}>
                            <View style={{flexDirection: 'row'}}>
                                <Text>{info.name}</Text>
                            </View>
                            <View style={{alignItems:'center', marginTop:5, flexDirection: 'row'}}>
                                <Image style={{marginLeft:2, width:10, height:14, marginRight:8}} source={images.iconGpsBlack} />
                                <View style={{flex:0.8}}>
                                    <Text>{info.location.street}</Text>
                                </View>
                            </View>
                            <View style={{alignItems:'center', marginTop:5, flexDirection: 'row'}}>
                                <Image style={{ width:14, height:14, marginRight:8}} source={images.clockIcon} />
                                <Text>End Hour {info.endHour}</Text>
                            </View>   
                            </View>
                            <View style={{alignItems:'center', paddingHorizontal:10, paddingVertical:20, flexDirection: 'column'}}>
                                <Text style={{fontSize:12, color:'grey'}}>{language.congestionInfo}</Text>
                                <Text style={{fontSize:12, color:'grey'}}>> 30 min </Text>
                                <View style={{marginTop:5, width:30, height:30, backgroundColor:'#DF6E6E', borderRadius:50}} />
                            </View>
                        </View>
                    </View>
                    <View style={{marginTop:20, alignItems:'center',  marginHorizontal:10}}>
                        <Image style={{width:width*0.95, height:170}} source={images.graficaEstadisticas} />
                    </View>
                </ScrollView>
          </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
     height:height,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });