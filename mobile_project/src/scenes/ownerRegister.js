import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ActivityIndicator,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  Input,
  Alert,
  Button,
  SafeAreaView,
  TextInput
} from 'react-native';

import images from '@assets/imageGeneral'
import RNPickerSelect from 'react-native-picker-select';
import apiRoute from '@routes/apiRoute';
import language from '@languages/languagesGeneral'

const { width, height } = Dimensions.get('window');



const places = [
    {
      label: 'Pharmacie',
      value: 'Pharmacie',
    },
    {
      label: 'Hospital',
      value: 'Hospital',
    },
    {
      label: 'Grocery',
      value: 'Grocery',
    },
    {
      label: 'Bakery',
      value: 'Bakery',
    },
    {
      label: 'Supermarket',
      value: 'Supermarket',
    },
]

export default class OwnerRegister extends Component {

  constructor(props) {
    super(props);
    this.state = {
        email: "",
        storeName: "",
        address: "",
        password: ""
    };
  }

componentDidMount() {
    
}

goBack() {
    const { navigation } = this.props;
    navigation.goBack();
}

async verifyData(){
    Keyboard.dismiss
    this.setState({loading: true})
    const {email,password, storeName, address} = this.state;

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    
    const userEmail = email
    
    if(userEmail == undefined || userEmail == ""){
        alert("Email requerido")
        this.setState({loading: false})
    } else if(password == undefined || password == ""){
        alert("Password requerido")
        this.setState({loading: false})
    } else if(storeName == undefined || storeName == ""){
        alert("Store name require")
        this.setState({loading: false})
    } else if(address == undefined || address == ""){
        alert("Address require")
        this.setState({loading: false})
    }  else if(userEmail != undefined && userEmail != "" && reg.test(userEmail) === false) {
        alert('Email syntax invalid, it must to be like this: myemail@email.com');
        this.setState({loading: false})
    } else{
        const email = userEmail.toLowerCase();
        this.setState({emailLower: email})
        this.callLoginAPI(false)
        setTimeout(() => { 
          this.callLoginAPI(true)
      },1000);
    }
}

callLoginAPI(process) {
    if(process){
    var finalUrl = apiRoute.api + "owners/signup"
    console.log("finalUrl", finalUrl)
    //console.warn("finalUrl", finalUrl)
    try{
      fetch(finalUrl,{
          method: 'POST',
          headers:{
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: this.state.emailLower,
            password: this.state.password,
          }),
          }).then((response) => {
            if(response.status == 200 || response.status == 201){
              this.setState({loading: false})
              response.json().then((json) => {
                console.log("LOG", json.success)
                if(!json.success) {
                  alert(json.error)
                } else {
                  Alert.alert(
                    "Success",
                    json.user.email + " "+ language.hasBeenRegistered,
                    [
                      {
                        text: language.automaticLogin,
                        onPress: () => this.loginUser(this.state.emailLower, this.state.password)
                      },
                      { text: language.manualLogin, onPress: () => this.props.navigation.navigate('OwnerLogin') }
                    ],
                    { cancelable: false }
                  );
                  //alert("SUCCESS " + json.user.email + " has been registered")
                  // setTimeout(() => { 
                  //     this.setState({loading: true})
                  //     this.loginUser(this.state.emailLower, this.state.password)
                  // },3500);
                  //this.props.navigation.navigate('OwnerLogin', {emailValue: this.state.email, passwordValue: this.state.password})
                }
              })
            }
          })
        }catch(error){
          console.warn(error)
        }
      }
    } 

    loginUser(email, password){
      this.setState({loading: true})
      var finalUrl = apiRoute.api + "owners/signin"
      try{
          fetch(finalUrl,{
              method: 'POST',
              headers:{
              Accept: 'application/json',
              'Content-Type': 'application/json',
              },
              body: JSON.stringify({
              email: email,
              password: password,
              }),
              }).then((response) => {
              if(response.status == 200 || response.status == 201){
                  this.setState({loading: false})
                  response.json().then((json) => {
                  console.log("LOG", json)
                  if(!json.success) {
                      alert(json.error)
                  } else {
                    Alert.alert(
                      language.success,
                      this.state.emailLower + " " + language.hasBeenRegistered,
                      [
                        {
                          text: "OK",
                          onPress: () => this.props.navigation.navigate('OwnerPlatform')
                        }
                      ],
                      { cancelable: false }
                    );
                  }
                  })
              }
              })
          }catch(error){
              console.warn(error)
          }
      } 


      
  render() {

    const createThreeButtonAlert = () =>
    Alert.alert(
      "Alert Title",
      "My Alert Msg",
      [
        {
          text: "Ask me later",
          onPress: () => console.log("Ask me later pressed")
        },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ],
      { cancelable: false }
    );

    const { state, goBack } = this.props.navigation;
    return (
      <View style={{backgroundColor:'white', flex:1}}>
        <SafeAreaView>
            <TouchableOpacity 
                onPress={() => this.goBack()}
                style={{marginLeft:20, marginTop:10}}>
                <Image style={{ width:20, height:30}} source={images.backIcon} />      
            </TouchableOpacity>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View>
                    <View style={{flexDirection:'column', alignItems:'center', marginHorizontal:20}}>
                        <Text style={{fontSize:22}}>{language.ownerRegister}</Text>
                        <Text style={{marginTop:10, textAlign:'center', color:'grey'}}>{language.pleaseFillForm}</Text>
                        <TextInput 
                            autoCorrect={true}
                            onChangeText={storeName => this.setState({storeName})}
                            placeholder={language.storeName}
                            style={{color:'grey',paddingHorizontal:15, marginTop:20, borderRadius:5, borderWidth:0.3, borderColor:'#CCC', height: 40, width: width*0.8, backgroundColor:'#FCFCFC'}}>
                        </TextInput>
                        <TextInput 
                            autoCorrect={true}
                            onChangeText={address => this.setState({address})}
                            placeholder={language.storeAddress}
                            style={{color:'grey',paddingHorizontal:15, marginTop:20, borderRadius:5, borderWidth:0.3, borderColor:'#CCC', height: 40, width: width*0.8, backgroundColor:'#FCFCFC'}}>
                        </TextInput>
                        <TextInput 
                            autoCorrect={false}
                            onChangeText={email => this.setState({email})}
                            placeholder="Email"
                            style={{color:'grey',paddingHorizontal:15, marginTop:20, borderRadius:5, borderWidth:0.3, borderColor:'#CCC', height: 40, width: width*0.8, backgroundColor:'#FCFCFC'}}>
                        </TextInput>
                        <TextInput 
                            onChangeText={password => this.setState({password})}
                            placeholder="Password"
                            secureTextEntry={true}
                            style={{color:'grey',paddingHorizontal:15, marginTop:20, borderRadius:5, borderWidth:0.3, borderColor:'#CCC', height: 40, width: width*0.8, backgroundColor:'#FCFCFC'}}>
                        </TextInput>

                        <View style={{ justifyContent: 'center',  
                                                marginTop: 20,
                                                borderRadius:5,
                                                borderWidth:0.25, 
                                                borderColor: '#CCC',
                                                width:width*0.8, height:40}}>
                            <View style={{marginLeft:10}}>
                                <RNPickerSelect

                                    placeholder={{}}
                                    onValueChange={(value) => {console.warn(value)}}
                                    items={places}
                                    Icon={() => {
                                        return <Image style={{marginRight:5, width:25, height:20}} source={images.downIcon} /> ;
                                        }}
                                />
                            </View>
                        </View> 
                        {this.state.loading ?
                        <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                        :
                        <View style={{alignItems:'center'}}>
                          <TouchableOpacity style={{marginTop:20}}>
                              <Text>{language.setLocation}</Text>
                          </TouchableOpacity>
                          <TouchableOpacity 
                          onPress={()=>{this.verifyData()}}
                          style={{marginTop:10, borderRadius:5, alignItems:'center', justifyContent:'center', height:40, width:width*0.8, backgroundColor:'#3796F0'}}>
                              <Text style={{color:'white'}}>{language.register}</Text>
                          </TouchableOpacity>
                        </View>
                        }
                    </View>
                </View>
            </TouchableWithoutFeedback>
          </SafeAreaView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });