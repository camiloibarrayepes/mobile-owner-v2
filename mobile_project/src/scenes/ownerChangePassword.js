import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Picker,
  ActivityIndicator,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  Input,
  Alert,
  Button,
  SafeAreaView,
  TextInput
} from 'react-native';

import images from '@assets/imageGeneral'
import apiRoute from '@routes/apiRoute';
import language from '@languages/languagesGeneral'
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('window');

export default class OwnerChangePassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
        repeatNewPass: "",
        actualPass: "",
        newPass: "",
        password: "",
        tokenUser: "",
        userPass: ""
    };
  }

componentDidMount() {
    this.getData()
}

goBack() {
    const { navigation } = this.props;
    navigation.goBack();
}

getData = async () => {
    try {
      const tokenStorage = await AsyncStorage.getItem('token')
      console.warn("tokenStorage", tokenStorage)
      //
      const userPass = await AsyncStorage.getItem('userPass')
      console.warn("userPass", userPass)
      //
      this.setState({
          tokenUser: tokenStorage,
          userPass: userPass
        })
    } catch(e) {
      //error
    }
  }

async verifyData(){
    Keyboard.dismiss
    this.setState({loading: true})
    const {repeatNewPass, actualPass, newPass} = this.state;

    //let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    
    const userEmail = repeatNewPass
    
    if(actualPass == undefined || actualPass == "") {
        alert("Contraseña actual requerida")
        this.setState({loading: false})
    } else if(newPass == undefined || newPass == "") {
        alert("Nueva contraseña requerida")
        this.setState({loading: false})
    } else if(repeatNewPass == undefined || repeatNewPass == "") {
        alert("Repetir contraseña")
        this.setState({loading: false})
    } else if (actualPass != this.state.userPass) {
        alert("Contraseña actual no coincide")
        this.setState({loading: false})
    } else {
        if (newPass != repeatNewPass) {
            this.setState({loading: false})
            alert("Contraseñas no coinciden")
        } else {
            if(this.state.tokenUser == "") {
                console.warn("1")
                this.setState({loading: true})
            } else {
                console.warn("2", this.state.tokenUser)
                setTimeout(() => { 
                this.callLoginAPI(newPass)
              },1000);
            }
        }
    }
}

callLoginAPI(newPass) {
    console.warn("newPass", newPass)
    var finalUrl = apiRoute.api + "owners/password"
    console.log("finalUrl", finalUrl)
    try{
      fetch(finalUrl,{
          method: 'PUT',
          headers:{
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': this.state.tokenUser
          },
          body: JSON.stringify({
            password: newPass
          }),
          }).then((response) => {
            console.warn("responsex3", response)
            if(response.status == 200 || response.status == 201){
              this.setState({loading: false})
              response.json().then((json) => {
                console.log("LOGx2", json.success)
                
                if(!json.success) {
                  alert(json.error)
                } else {
                    alert("Contraseña cambiada con éxito")
                    this.goBack()
                }
              })
            }
          })
        }catch(error){
          console.warn(error)
        }
    } 

    loginUser(email, password){
      this.setState({loading: true})
      var finalUrl = apiRoute.api + "owners/signin"
      try{
          fetch(finalUrl,{
              method: 'POST',
              headers:{
              Accept: 'application/json',
              'Content-Type': 'application/json',
              },
              body: JSON.stringify({
              email: email,
              password: password,
              }),
              }).then((response) => {
              if(response.status == 200 || response.status == 201){
                  this.setState({loading: false})
                  response.json().then((json) => {
                  console.log("LOG", json)
                  if(!json.success) {
                      alert(json.error)
                  } else {
                    Alert.alert(
                      language.success,
                      this.state.emailLower + " " + language.hasBeenRegistered,
                      [
                        {
                          text: "OK",
                          onPress: () => this.props.navigation.navigate('OwnerPlatform')
                        }
                      ],
                      { cancelable: false }
                    );
                  }
                  })
              }
              })
          }catch(error){
              console.warn(error)
          }
      } 


      
  render() {

    const createThreeButtonAlert = () =>
    Alert.alert(
      "Alert Title",
      "My Alert Msg",
      [
        {
          text: "Ask me later",
          onPress: () => console.log("Ask me later pressed")
        },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ],
      { cancelable: false }
    );

    const { state, goBack } = this.props.navigation;
    return (
      <View style={{backgroundColor:'white', flex:1}}>
        <SafeAreaView>
            <TouchableOpacity 
                onPress={() => this.goBack()}
                style={{marginLeft:20, marginTop:20}}>
                <Image style={{ width:25, height:25}} source={images.exitIcon} />      
            </TouchableOpacity>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View>
                    <View style={{flexDirection:'column', alignItems:'center', marginHorizontal:20}}>
                        <Text style={{fontSize:22}}>{language.changePass}</Text>
                        <TextInput 
                            autoCapitalize = "none"
                            autoCorrect={true}
                            onChangeText={actualPass => this.setState({actualPass})}
                            placeholder={language.actualPass}
                            style={{color:'grey',paddingHorizontal:15, marginTop:20, borderRadius:5, borderWidth:0.3, borderColor:'#CCC', height: 40, width: width*0.8, backgroundColor:'#FCFCFC'}}>
                        </TextInput>
                        <TextInput 
                            autoCapitalize = "none"
                            autoCorrect={true}
                            onChangeText={newPass => this.setState({newPass})}
                            placeholder={language.newPass}
                            style={{color:'grey',paddingHorizontal:15, marginTop:20, borderRadius:5, borderWidth:0.3, borderColor:'#CCC', height: 40, width: width*0.8, backgroundColor:'#FCFCFC'}}>
                        </TextInput>
                        <TextInput 
                            autoCapitalize = "none"
                            autoCorrect={false}
                            onChangeText={repeatNewPass => this.setState({repeatNewPass})}
                            placeholder={language.repeatNewPass}
                            style={{color:'grey',paddingHorizontal:15, marginTop:20, borderRadius:5, borderWidth:0.3, borderColor:'#CCC', height: 40, width: width*0.8, backgroundColor:'#FCFCFC'}}>
                        </TextInput>
                        {this.state.loading ?
                        <ActivityIndicator style={{marginTop:20}} size="large" color="grey" />
                        :
                        <View style={{alignItems:'center'}}>
                          <TouchableOpacity 
                          onPress={()=>{this.verifyData()}}
                          style={{marginTop:20, borderRadius:5, alignItems:'center', justifyContent:'center', height:40, width:width*0.8, backgroundColor:'#3796F0'}}>
                              <Text style={{color:'white'}}>{language.register}</Text>
                          </TouchableOpacity>
                        </View>
                        }
                    </View>
                </View>
            </TouchableWithoutFeedback>
          </SafeAreaView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
    button: {
      position: 'absolute',
      top: 20,
      padding: 10,
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    pickerStyle: {
        color:'red'
    }
  });